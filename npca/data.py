import re
import numpy as np

from .utils import to_regex, InvalidBlockException, DataFormatException
from .solve import find_components
from .model import Model

class DataNet:
    """
    Contains a collection of references to blocks (and indirectly to the groups they represent)
    Blocks must be added to the blocks member, as
    <dataset>.blocks.<blockname> = <Block object>
    """
    def __init__(s):
        s.blocks    = DatasetContainer() # All blocks must be declared here
        s.groups    = DatasetContainer() # Used to keep named references to Group objects
        s.plot_sets = DatasetContainer() # Used to keep named references to PlotSet objects
    
    # Generation methods
    def add_multiple(s, method, common_args, *args):
        if common_args is None:
            common_args = {}
        for next_args in args:
            group_args = {}
            group_args.update(**common_args)
            group_args.update(**next_args)
            method(**group_args)
    def add_plot_sets(s, *args, common_args=None):
        s.add_multiple(s.add_plot_set, common_args, *args)
    def add_groups(s, *args, common_args=None):
        s.add_multiple(s.add_group, common_args, *args)
    def add_blocks(s, *args, common_args=None):
        s.add_multiple(s.add_block, common_args, *args)

    def add_plot_set(s, key, label, color=(100, 100, 100)):
        s.plot_sets.put(key, PlotSet(key, label, color))
    def add_group(s, key, label, data_table, id_property, filter_property, filter_function, variables=True, **group_parameters):
        if variables:
            source = data_table.variable_metadata
        else:
            source = data_table.observation_metadata
        filter_selection = data_table.filter_mask(source, filter_property, filter_function)
        ids = source[id_property].values[filter_selection]
        metadata = {
            key: source[key].values[filter_selection]
            for key in source
            if key != id_property
        }
        group = Group(key, label, ids, metadata, filter_property, filter_function, **group_parameters)
        s.groups.put(key, group)
    def add_block(s, key, observation_group, variable_group, plot_set, data_table,
            override_observation_filter_property=None, override_variable_filter_property=None,
            override_observation_filter_function=None, override_variable_filter_function=None,
            relative_block_charge=1.0):
        if override_observation_filter_function is None:
            override_observation_filter_function = observation_group.filter_function
        if override_observation_filter_property is None:
            override_observation_filter_property = observation_group.filter_property
        if override_variable_filter_function is None:
            override_variable_filter_function = variable_group.filter_function
        if override_variable_filter_property is None:
            override_variable_filter_property = variable_group.filter_property
        
        variable_selection = data_table.variable_mask(override_variable_filter_property, override_variable_filter_function)
        observation_selection = data_table.observation_mask(override_observation_filter_property, override_observation_filter_function)
        block_data = data_table.data[observation_selection][:, variable_selection]
        if key is None:
            base_key = re.sub(r'(\A[^A-Za-z_])|[^A-Za-z0-9_]', '', f'{observation_group.key}_{variable_group.key}').lower()
            index = 0
            key = base_key
            while key in s.blocks.__dict__:
                index += 1
                key = f'{base_key}_{index}'
        block = Block(key, observation_group, variable_group, plot_set, block_data, relative_block_charge)
        s.blocks.put(key, block)

    # Access methods
    def get_groups(s, regex=None, observations=True, variables=True):
        """
        Retrieves the set of all groupd referenced by the blocks of the dataset,
        optionally switching off <observations> or <variables>
        Possibly filters their labels by <regex>, either a string to match exactly, or a compiled regex
        """
        blocks = s.get_blocks()
        all = []
        if observations:
            all = all + [block.observations for block in blocks]
        if variables:
            all = all + [block.variables for block in blocks]
        all = list(set(all)) # Remove duplicates
        if regex is None:
            return all
        else:
            regex_obj = to_regex(regex)
            return [group for group in all if regex_obj.fullmatch(group.name)]

    def get_blocks(s, regex=None):
        """
        Returns all blocks in this dataset
        Possibly filters the labels by <regex>, either a string to match exactly, or a compiled regex
        """
        all = s.blocks.as_list()
        if regex is None:
            return all
        else:
            regex_obj = to_regex(regex)
            return [block for block in all if regex_obj.fullmatch(block.name)]

    def remove_blocks(s, delete_list):
        """
        Removes all the references in the dataset (s.blocks) to elements in <delete_list>
        """
        attribute_names = []
        for name, block in s.blocks.__dict__.items():
            if block in delete_list:
                attribute_names.append(name)
        for name in attribute_names:
            delattr(s.blocks, name)

    # Preparation methods
    def normalize_observations(s, reference_blocks=None, norm_power=1):
        """
        Centers and normalizes every set of blocks with the same variables group
        <norm_power> is the power used for normalization (1 is UV, 0.5 is Pareto)
        <reference_blocks> are used to define the center and scale, and
        can either be None (all blocks are used), an explicit list of Block objects,
        or a regex string
        """
        variables = s.get_groups(observations=False)
        blocks = s.get_blocks()
        blocks_by_variables = {group: [] for group in variables}
        for block in blocks:
            block.data = block.data * block.scale + block.center
            blocks_by_variables[block.variables].append(block)
        all_reference_blocks = blocks
        if isinstance(reference_blocks, list):
            all_reference_blocks = reference_blocks
        elif isinstance(reference_blocks, str):
            all_reference_blocks = s.get_blocks(regex=re.compile(reference_blocks))
        for variables, to_normalize in blocks_by_variables.items():
            references = [b for b in to_normalize if b in all_reference_blocks]
            if len(references) == 0:
                raise InvalidBlockException('Provided reference_blocks did not produce enough blocks for variable group {}'.format(variables.name))
            sums = sum([np.sum(block.data, axis=0, keepdims=True) for block in references])
            sums_sq = sum([np.sum(np.power(block.data, 2), axis=0, keepdims=True) for block in references])
            sums_n = sum([block.data.shape[0] for block in references])
            mean = sums / sums_n
            mean_sq = sums_sq / sums_n
            std = np.sqrt(mean_sq - np.power(mean, 2))
            for block in to_normalize:
                block.center = mean
                block.scale = np.power(std, norm_power)
                block.data = (block.data - block.center) / block.scale
                block.validate()
    
    def build_charges(s, homogeneous_observations=True, homogeneous_variables=True, data_function=None):
        """
        Sets the fit charges for all the blocks
        If <homogeneous_observations>, blocks are weighted for their size so all observation groups have the same influence
        Similar for <homogeneous_variables>
        The optional <data_function> takes three arguments, (<data>, <obs_metadata>, <var_metadata>), and should
        produce a float giving further weight corrections
        """
        for b in s.get_blocks():
            charge = b.relative_charge
            if homogeneous_observations:
                charge /= np.power(b.observations.size(), 0.25)
            if homogeneous_variables:
                charge /= np.power(b.variables.size(), 0.25)
            if data_function is not None:
                charge *= data_function(b.data * b.scale + b.center, b.observations.metadata, b.variables.metadata)
            b.charge = charge

    # Model methods
    def model(s, n_components=2, **solver_args):
        """
        Builds and returns a Model object with <n_components> for the current dataset
        See solve.find_component for more information on <solver_args>
        """
        return Model(s, find_components(s, n_components, **solver_args))

    def variance(s, unweighted=False, per_variable=False):
        """
        Returns the sum of all variances in the blocks of this dataset
        """
        blocks = s.get_blocks()
        if per_variable:
            variable_groups = s.get_groups(observations=False, variables=True)
            result = {group: 0 for group in variable_groups}
            for block in blocks:
                result[block.variables] = block.variance(unweighted, True) + result[block.variables]
            return result
        else:
            result = 0
            for block in blocks:
                result += block.variance(unweighted, False)
            return result

class DatasetContainer:
    """
    Used to hold references as member variables
    """
    def as_list(s):
        """
        Returns all members of this object as a list
        """
        return list(s.__dict__.values())
    
    def clear(s):
        s.__dict__.clear()
    
    def member_names(s):
        return list(s.__dict__.keys())
    
    def put(s, key, value):
        s.__dict__[key] = value
    
    def __getitem__(s, key):
        return s.__dict__[key]

class Group:
    """
    Contains a group of observation types (individuals, features, time points...)
    Takes the <label> of the group and a list of <ids> for each observation, which also determines the length of the group
    and an <order> of precedence where higher values come first
    """
    def __init__(s, key, label, ids, metadata, filter_property, filter_function, order=0):
        s.label = label # string
        s.key = key # string
        s.ids = ids # list of strings
        s.order = order # int describing the order of the group
        s.metadata = {key: values for key, values in metadata.items()}
        s.filter_property = filter_property
        s.filter_function = filter_function

    def size(s):
        """
        Size of group
        """
        return len(s.ids)

    def __deepcopy__(s, memo):
        """
        Group objects should not be cloned, so that they can be used universally as references
        """
        return s

class Block:
    """
    Contains a data block as a numpy matrix
    Takes the <key> of the block, the <observations_group> and <variables_group>
    for each dimension of the matrix, and the actual <data> matrix
    The <plot_set> object is used for plotting attributes, if not None
    The dimensions of the matrix must match (observations, variables)
    An optional relative <relative_charge> can be specified, to be used during the model fitting
    """
    def __init__(s, key, observations_group, variables_group, plot_set, data, relative_block_charge):
        s.key = key # string
        s.observations = observations_group # Group object reference
        s.variables = variables_group # Group object reference
        s.data = data # np matrix
        s.relative_charge = relative_block_charge # float, block weight on models
        s.plot_set = plot_set # PlotSet object for visualization
        s.validate()
        s.center = np.zeros((1, variables_group.size())) # internal: relationship to "raw" data
        s.scale = np.ones((1, variables_group.size())) # internal: relationship to "raw" data
        s.charge = relative_block_charge # internal: actual block weight, modifiable by dataset normalization

    def validate(s):
        """
        Check if the dimensions of the data matrix match the sizes of the related groups,
        Should be called whenever the member variables are changed
        """
        if len(s.data.shape) != 2:
            raise DataFormatException("Block '{}' is not a matrix (shape {})".format(s.name, s.data.shape))
        if s.data.shape[0] != s.observations.size():
            raise DataFormatException("Block '{}' has inconsistent data size ({}) for observations group {} ({})".format(
                s.key, s.data.shape[0], s.observations.label, s.observations.size()
            ))
        if s.data.shape[1] != s.variables.size():
            raise DataFormatException("Block '{}' has inconsistent data size ({}) for variables group {} ({})".format(
                s.key, s.data.shape[1], s.variables.label, s.variables.size()
            ))

    def variance(s, unweighted, per_variable):
        charge = 1 if unweighted else s.charge
        if per_variable:
            return np.sum(np.power(charge * s.data, 2), axis=0)
        return np.sum(np.power(charge * s.data, 2))

class PlotSet:
    """
    Contains plotting information for blocks
    """
    def __init__(s, key, label, color):
        s.key = key
        s.label = label
        s.color = color
