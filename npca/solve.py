import time as tm
import numpy as np

from .model import Component
from .utils import normalize

from ..io.display import NullMonitor

def find_components(dataset, n_components, monitor=None, **solver_args):
    """
    Returns a list holding <n_components> Component objects
    For information on <solver_args>, see find_component
    """
    if monitor is None:
        monitor = NullMonitor()
    blocks = dataset.get_blocks()

    for block in blocks:
        block.validate()

    deflated_blocks = {block: np.copy(block.data) for block in blocks}

    components = []
    for component_id in range(0, n_components):
        comp = find_component(component_id, dataset, deflated_blocks, components, monitor, **solver_args)
        for block in blocks:
            deflated_blocks[block] = deflated_blocks[block] - comp.block_reconstruct(block)
        components.append(comp)
    return components

def find_component(i_component, dataset, data_matrices, previous_components, monitor,
        dependants=None, normalize_dependants=False,
        min_inner_iterations=5, max_inner_iterations=1000, min_outer_steps=5, max_outer_steps=200,
        tolerance=1e-6, orthogonalize=False, consensus_signs=False):
    """
    Find a single Component object for the given <dataset> and <data_matrices>
    -The <data_matrices> must be in one-to-one correspondence with the data blocks of the <dataset>
    -Performs several steps of minimization (at least <min_outer_steps> and at most <max_outer_steps>),
    until the maximal change of any loading through the step is lower than <tolerance>
    -Each step solves a generalized eigenvalue problem by iterating (at least <min_inner_iterations> and at most <max_inner_iterations>),
    until the maximal change of any loading through the step is lower than <tolerance>
    -A list of <previous_components> Component objects can be specified for orthogonalization, applied if <orthogonalize> is True
    -A dict of <dependants> can be specified for supervised fitting, mapping component indices to dicts of (Blocks or PlotSet objects to
    dependant variable weights), which can be normalized between blocks if <normalize_dependants> is True
    -The function <print_callback> takes one string argument, and is supplied used-readable iteration updates
    """    
    start_time = tm.time()
    blocks = dataset.get_blocks()
    observations = dataset.get_groups(variables=False)
    variables = dataset.get_groups(observations=False)
    blocks_by_observations = {o: [b for b in blocks if b.observations == o] for o in observations}
    blocks_by_variables = {v: [b for b in blocks if b.variables == v] for v in variables}
    blocks_by_groups = {(o, v): [b for b in blocks if (b.variables == v and b.observations == o)] for o in observations for v in variables}
    orthogonal_components = []
    if orthogonalize:
        orthogonal_components = previous_components
    if previous_components is None:
        previous_components = []
    if dependants is None:
        dependants = []

    observation_loadings = {group: normalize(np.ones(group.size())) for group in observations}
    variable_loadings = {group: normalize(np.ones(group.size())) for group in variables}

    charged_data_matrices = {b: b.charge * data_matrices[b] for b in data_matrices}

    def broadcast_dependants(block_or_plot_set, dependant_value):
        if block_or_plot_set in blocks:
            return {block_or_plot_set: dependant_value * np.ones(block_or_plot_set.observations.size())}
        else:
            return {block: dependant_value * np.ones(block.observations.size()) for block in blocks if block.plot_set == block_or_plot_set}

    def normalize_dependant_dict(dependants_by_block):
        norm = np.sqrt(sum([np.sum(np.power(dependants_by_block[b], 2)) for b in dependants_by_block]))
        return {b: dependants_by_block[b] / norm for b in dependants_by_block}

    normalized_dependants = {}
    if i_component in dependants:
        normalized_dependants = {
            block: broadcasted_dependants
            for block_or_plot_set, dependant_value in dependants[i_component].items()
            for block, broadcasted_dependants in broadcast_dependants(block_or_plot_set, dependant_value).items()
        }
        if normalize_dependants:
            normalized_dependants = normalize_dependant_dict(normalized_dependants)

    def build_observations_matrix(variable_loadings):
        def fill_function(h, g, submatrix):
            if h == g:
                for block in blocks_by_observations[h]:
                    submatrix.add(ProjectorMatrix(block, charged_data_matrices, variable_loadings, project_observations=False))
        return IterationMatrix(observations, fill_function, orthogonal_components)

    def build_variables_matrix(observation_loadings):
        def fill_function(h, g, submatrix):
            if h == g:
                for block in blocks_by_variables[h]:
                    submatrix.add(ProjectorMatrix(block, charged_data_matrices, observation_loadings, project_observations=True))
            for block_a in normalized_dependants:
                for block_b in normalized_dependants:
                    if block_a.variables == h and block_b.variables == g:
                        submatrix.add(DependantMatrix(block_a, block_b, charged_data_matrices, normalized_dependants, 1))
        return IterationMatrix(variables, fill_function, orthogonal_components)

    def iterate(input_loadings, iterate_matrix, callback_text):
        current_loadings = {g: input_loadings[g] for g in input_loadings}
        iterations = 0
        iteration_change = float('inf')
        max_change = float('-inf')
        while iterations < min_inner_iterations or (iterations < max_inner_iterations and iteration_change > tolerance):
            iterations += 1
            current_loadings, _, _, iteration_change = iterate_matrix.apply(current_loadings)
            if iteration_change > max_change:
                max_change = iteration_change
        monitor.state('\t{}: {} iterations, max change {:.1f} tols'.format(callback_text, iterations, max_change / tolerance))
        return current_loadings, max_change

    def fix_variable_signs(loadings):
        for g in variables:
            sign_contributions = 0
            for block in blocks:
                if block.variables == g:
                    scores = np.dot(block.data, loadings[g])
                    means = np.mean(block.data, axis=1)
                    sign_contributions += np.dot(scores, means)
            if sign_contributions < 0:
                loadings[g] = -loadings[g]
        if consensus_signs:
            def variable_group_order(g):
                total_score_variance = 0
                for b in blocks_by_variables[g]:
                    total_score_variance += np.linalg.norm(np.dot(b.data, loadings[g]))
                return -total_score_variance
            sorted_variables = sorted(variables, key=variable_group_order)
            for g_i, _ in enumerate(sorted_variables):
                g = sorted_variables[g_i]
                sign_contributions = 0
                for h_i in range(g_i):
                    h = sorted_variables[h_i]
                    for o in observations:
                        for b in blocks_by_groups[(o, g)]:
                            for c in blocks_by_groups[(o, h)]:
                                scores_g = np.dot(b.data, loadings[g])
                                scores_h = np.dot(c.data, loadings[h])
                                sign_contributions += np.dot(scores_g, scores_h)
                if sign_contributions < 0:
                    loadings[g] = -loadings[g]

    def estimate_weight(loadings, block):
        result = data_matrices[block]
        result = np.dot(result, loadings[block.variables])
        result = np.dot(result, loadings[block.observations])
        return result

    steps = 0
    step_change = float('inf')
    while steps < min_outer_steps or (steps < max_outer_steps and step_change > tolerance):
        steps += 1
        monitor.state('PC{}: loadings step {}'.format(i_component + 1, steps))
        observation_loadings, obs_change = iterate(observation_loadings, build_observations_matrix(variable_loadings), 'Observations')
        variable_loadings, var_change = iterate(variable_loadings, build_variables_matrix(observation_loadings), 'Variables')
        step_change = max(obs_change, var_change)
    fix_variable_signs(variable_loadings)
    monitor.state('PC{}: weights'.format(i_component + 1))
    all_loadings = {}
    for g in observations:
        all_loadings[g] = observation_loadings[g]
    for g in variables:
        all_loadings[g] = variable_loadings[g]
    all_weights = {block: estimate_weight(all_loadings, block) for block in blocks}

    end_time = tm.time()
    monitor.log('PC{}: ran in {:.3f}s'.format(i_component + 1, end_time - start_time))
    monitor.state('Done')
    return Component(all_loadings, all_weights)

class IterationMatrix:
    """
    Represents the iteration matrix that is applied to the set of group loadings corresponding to <groups>
    on each step of the generalized eigenvalue iteration
    The function <fill_function> is used to fill in the IterationMatrix, 
    and takes three arguments, an "out" loading, an "in" loading, and a SubIterationMatrix corresponding
    to the linear mapping from "in" to "out" to which contributions can be added
    <orthogonal_components> is a list (possibly empty) of Component objects w.r.t. which the computed loadings are orthogonalized
    """
    def __init__(s, groups, fill_function, orthogonal_components):
        s.groups = groups
        s.orthogonal_components = orthogonal_components
        s.submatrices = {h: {g: SubIterationMatrix() for g in groups} for h in groups}
        for h in groups:
            for g in groups:
                fill_function(h, g, s.submatrices[h][g])

    def apply(s, old_loadings):
        """
        Applies this IterationMatrix on a dict <old_loadings> of Group objects to loading arrays
        orthogonalizing if applicable
        Returns a tuple with the result of the application, the variations in the loadings, the group
        with the highest variation, and its change
        """
        new_loadings = {h: normalize(sum([s.submatrices[h][g].apply(old_loadings[g]) for g in s.groups])) for h in s.groups}
        for group in new_loadings:
            ortho = new_loadings[group]
            for component in s.orthogonal_components:
                ortho = ortho - np.dot(ortho, component.loadings[group]) * component.loadings[group]
                ortho = normalize(ortho)
            new_loadings[group] = ortho
        changes = {h: 0.5 * (1 - np.dot(old_loadings[h], new_loadings[h])) for h in s.groups}
        max_change_group = max(changes, key=lambda h: changes[h])
        return new_loadings, changes, max_change_group, changes[max_change_group]

class SubIterationMatrix:
    """
    Represents an sub-iteration matrix, mapping one loading to another, as a sum of contributions
    The matrix itself is not explicitly computed, so that the contributions do not
    need to be compiled (useful for high-dimensional but low-rank matrices)
    Instead, whenever the matrix is to be applied on an object, the contributions are
    applied one by one and then summed over
    """
    def __init__(s):
        s.contributions = []

    def add(s, contribution_matrix):
        """
        Adds a contribution object <contribution_matrix> (must have an apply method) to the list
        """
        s.contributions.append(contribution_matrix)

    def apply(s, loading):
        """
        Applies this iteration matrix to the given <loading>,
        by applying each contribution on it and summing over
        """
        return np.sum([contribution_matrix.apply(loading) for contribution_matrix in s.contributions], axis=0)

class ProjectorMatrix:
    """
    Represents a PCA-like contribution of a Block object <block> to the IterationMatrix by projecting the loading
    of <block> onto the data to compute scores, and using the projector on those scores as the iterator
    <loadings> is a dict from Block to loading arrays
    <charged_data_matrices> is a dict from Block to data arrays
    <project_observations> determines whether the observation loadings or the variable loadings are projected
    """
    def __init__(s, block, charged_data_matrices, loadings, project_observations=True):
        if project_observations:
            s.project_vector = np.dot(loadings[block.observations], charged_data_matrices[block])
        else:
            s.project_vector = np.dot(charged_data_matrices[block], loadings[block.variables])

    def apply(s, loading):
        return s.project_vector * np.dot(s.project_vector, loading)

class CorrelationMatrix:
    def __init__(s, block, charged_data_matrices, weight):
        s.apply_matrix = weight * charged_data_matrices[block]

    def apply(s, loading):
        intermediate = np.dot(s.apply_matrix, loading)
        return np.dot(intermediate, s.apply_matrix)

class DependantMatrix:
    """
    Represents a PLS-like contribution between a couple of Block objects <block_a> and <block_b>
    by projecting against their correlations with the given dependants
    <charged_data_matrices> is a dict from Block to data arrays
    <dependants_by_block> is a list from Block to dependant arrays
    """
    def __init__(s, block_a, block_b, charged_data_matrices, dependants_by_block, weight):
        s.project_in = weight * np.dot(dependants_by_block[block_b], charged_data_matrices[block_b])
        s.project_out = weight * np.dot(dependants_by_block[block_a], charged_data_matrices[block_a])

    def apply(s, loading):
        return s.project_out * np.dot(s.project_in, loading)
