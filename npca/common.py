import re as regex

from . import data
from . import utils

def new_data_net():
    """
    Wrapper for data.DataNet object creator
    """
    return data.DataNet()

def inspect(obj, max_level=0):
    """
    Returns a string with a tree description of <obj>
    with at most <max_level> depth
    """
    return utils.inspect(obj, max_level=max_level)

def re(pattern):
    """
    Convenience method to return a regex object
    """
    return regex.compile(pattern)
