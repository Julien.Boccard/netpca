function updateBlocks() {
  var selected_block_indices = block_map.data[0].selectedpoints
  if (!selected_block_indices) {
    selected_block_indices = []
  }
  var selected_blocks = selected_block_indices.map(i => block_map.data[0].customdata[i].index)
  shapes = []
  for (let cd of block_map.data[0].customdata) {
    var rect_data = Object.assign({}, cd.rect_data)
    if (selected_blocks.includes(cd.index)) {
      rect_data.fillcolor = 'rgb(240,240,240)'
    } else {
      rect_data.opacity = 0.5
    }
    shapes.push(rect_data)
  }
  Plotly.relayout(block_map, {'shapes': shapes})
}

function blockSelectionCallback(data) {
  if (data) {
    updateAll()
  }
}

function blockClickCallback(data) {
  if (data) {
    var current_selection = block_map.data[0].selectedpoints
    if (!current_selection) {
      current_selection = []
    }
    var click_index = data.points[0].pointIndex
    if (current_selection.includes(click_index)) {
      current_selection = current_selection.filter(i => i != click_index)
    } else {
      current_selection.push(click_index)
    }
    var data_update = {
      selectedpoints: [current_selection]
    }
    Plotly.update(block_map, data_update, {})
    updateAll()
  }
}

function blockLayoutCallback(data) {
  var min_size = 10
  var max_size = 200

  var w = block_map.layout.width
  var h = block_map.layout.height
  var min_x = block_map.layout.xaxis.range[0]
  var max_x = block_map.layout.xaxis.range[1]
  var min_y = block_map.layout.yaxis.range[0]
  var max_y = block_map.layout.yaxis.range[1]
  function x2p(x) {
    return w * (x - min_x) / (max_x - min_x)
  }
  function y2p(y) {
    return h * (y - min_y) / (max_y - min_y)
  }
  var blocks = block_map.data[0]
  var d2s = []
  for (var i=0; i<blocks.x.length; i++) {
    for (var j=i+1; j<blocks.x.length; j++) {
      var dx = x2p(blocks.x[i]) - x2p(blocks.x[j])
      var dy = y2p(blocks.y[i]) - y2p(blocks.y[j])
      d2s.push(dx*dx + dy*dy)
    }
  }
  d2s.sort((a,b) => a - b)
  var min_d = Math.sqrt(d2s[0])
  var size = Math.min(max_size, Math.max(min_size, min_d * 0.75))
  var marker_size = size * 0.9
  var line_size = size * 0.1
  var data_update = {
    'marker.size': marker_size,
    'marker.line.width': line_size
  }
  Plotly.restyle(block_map, data_update)
}
