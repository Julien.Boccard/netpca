from .utils import get_block_color

def make_block_map(model, monitor):
    import plotly.offline as po
    import plotly.graph_objs as go
    observations = sorted(model.dataset.get_groups(observations=True, variables=False), key=lambda g: (-g.order, g.label))
    variables = sorted(model.dataset.get_groups(observations=False, variables=True), key=lambda g: (-g.order, g.label))
    block_table = {o: {v: [] for v in variables} for o in observations}
    for b in model.dataset.get_blocks():
        block_table[b.observations][b.variables].append(b)
    max_blocks_by_observation = {o: max([len(block_table[o][v]) for v in variables]) for o in observations}

    x_pos = 0
    block_index = 0
    block_x = []
    block_y = []
    block_label = []
    block_color = []
    block_customdata = []
    block_hovertext = []
    block_indices = {}
    for _i_o, o in enumerate(observations):
        group_n_blocks = max_blocks_by_observation[o]
        y_pos = 0
        for _i_v, v in enumerate(variables):
            monitor.state(f'Blocks at {o.label}-{v.label}')
            for i_b, b in enumerate(block_table[o][v]):
                block_index_tag = f'block-{block_index}'
                block_plot_set = f' ({b.plot_set.label})' if b.plot_set is not None else ''
                block_color_string = get_block_color(b)
                block_x.append(x_pos + i_b)
                block_y.append(y_pos)
                block_label.append(f'<b>{b.key}</b>')
                block_color.append(block_color_string)
                block_customdata.append(dict(
                    index=block_index_tag,
                    name=b.key,
                    rect_data=dict(
                        type='rect',
                        x0=x_pos+i_b-0.45,  y0=y_pos-0.45, x1=x_pos+i_b+0.45, y1=y_pos+0.45,
                        line=dict(
                            color=block_color_string,
                            width=4,
                        ),
                        layer='below',
                        fillcolor=block_color_string
                    )
                ))
                block_hovertext.append(
                    f'<b>{b.key}</b>{block_plot_set}<br>'
                    f'- {b.observations.label}<br>'
                    f'- {b.variables.label}<br>'
                    f'Shape ({b.observations.size()} x {b.variables.size()})<br>'
                    f'Charge {b.relative_charge} ({b.charge})'
                )
                block_indices[b] = block_index_tag
                block_index += 1
            y_pos += 1
        x_pos += group_n_blocks + 1

    data = [go.Scatter(
        name='Block map',
        mode='markers+text',
        x=block_x,
        y=block_y,
        text=block_label,
        customdata=block_customdata,
        hovertext=block_hovertext,
        hoverinfo='text',
        selected=dict(textfont=dict(color='rgba(0,0,0,1.0)')),
        unselected=dict(textfont=dict(color='rgba(0,0,0,0.25)')),
        marker=dict(
            size=1,
            color='rgba(0,0,0,0)',
            symbol='square',
            line=dict(width=1, color='rgba(0,0,0,0)')
        )
    )]
    layout = go.Layout(
        title='Block map',
        dragmode='select',
        hovermode='closest',
        margin=go.layout.Margin(l=20, r=20, b=20, t=50, pad=10),
        template='plotly_white',
        xaxis=dict(
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            linecolor='rgb(180,180,180)',
            linewidth=3,
            mirror=True
        ),
        yaxis=dict(
            showgrid=False,
            zeroline=False,
            showticklabels=False,
            linecolor='rgb(180,180,180)',
            linewidth=3,
            mirror=True
        )
    )
    config = dict(
        modeBarButtons=[['select2d', 'pan2d', 'zoom2d', 'autoScale2d', 'toImage']],
        scrollZoom=True,
        toImageButtonOptions=dict(format='svg', filename='bpca-blocks'),
        displaylogo=False
    )
    fig = go.Figure(data=data, layout=layout)
    div = po.plot(fig, include_plotlyjs=True, output_type='div', config=config)
    return div, block_indices
