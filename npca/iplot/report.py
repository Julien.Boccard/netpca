import re
import subprocess as sp
import pkg_resources as pr

from .blocks import make_block_map
from .scores import make_score_traces
from .influences import make_influence_bars
from .comparisons import make_comparison_bars
from .utils import ExtendedEncoder

from ...io.display import NullMonitor

def report(model, outfile, min_variables_per_group=100, monitor=None, show=True):
    """
    Output a report of the given <model> into <outfile> as HTML
    <print_callback> is updated with the progress of the report generation
    """
    import dominate
    from dominate.tags import div, span, a, style, script, meta, input_, label
    from dominate.util import raw
    
    if monitor is None:
        monitor = NullMonitor()

    monitor.log('Building block map')
    block_map_div, block_indices = make_block_map(model, monitor)
    monitor.log('Computing scores')
    score_traces_div = make_score_traces(model, monitor, block_indices)
    monitor.log('Computing influences')
    influence_bars_div, influences_dict, block_influences_dict, variable_groups_dict, variable_group_indices, selected_numpy_variable_indices = make_influence_bars(model, monitor, block_indices, min_variables_per_group)
    monitor.log('Computing variable comparisons')
    variable_comparison_div, variable_comparison_dict = make_comparison_bars(model, monitor, block_indices, variable_group_indices, selected_numpy_variable_indices)
    monitor.state('Computations done')

    monitor.log('Building HTML document')
    doc = dominate.document(title='BPCA')
    with doc.head:
        meta(charset='utf-8')
        with style():
            raw(inject_resource('report.css'))
        with script():
            raw(inject_lines([
                inject_onload_scripts([
                    inject_js_reference('component-main-field', 'component_main_field'),
                    inject_js_reference('component-secondary-field', 'component_secondary_field'),
                    inject_js_reference('comparison-model-component', 'comparison_model_component'),
                    inject_js_reference('influences-shown-variables', 'influences_shown_variables'),
                    inject_js_reference('influences-name-filter', 'influences_name_filter'),
                    inject_js_reference('toggle-axis-scaling', 'toggle_axis_rescaling_checkbox'),
                    inject_js_reference('toggle-comparison-selected-blocks', 'toggle_comparison_selected_blocks'),
                    inject_js_reference('lock-influences', 'lock_influences'),
                    inject_js_reference('lock-scores', 'lock_scores'),
                    inject_js_reference('legend-scores', 'legend_scores'),
                    inject_js_reference('comparison-cumulative-model', 'comparison_cumulative_model'),
                    inject_js_reference('comparison-show-model', 'comparison_show_model'),
                    inject_js_reference('download-generator', 'download_generator'),
                    inject_js_reference('download-scores', 'download_scores'),
                    inject_js_reference('download-influences', 'download_influences'),
                    inject_js_reference('download-comparison', 'download_comparison'),
                    inject_js_reference('swap-axes-button', 'swap_axes_button'),
                    inject_plotly_reference(block_map_div, 'block_map'),
                    inject_plotly_reference(score_traces_div, 'score_traces'),
                    inject_plotly_reference(influence_bars_div, 'influence_bars'),
                    inject_plotly_reference(variable_comparison_div, 'variable_comparison_bars'),
                    inject_resource('report.js')
                ]),
                '\n',
                inject_json(influences_dict, 'influences'),
                inject_json(block_influences_dict, 'block_influences'),
                inject_json(variable_groups_dict, 'variable_groups'),
                inject_json(variable_comparison_dict, 'variable_comparisons'),
                '\n',
                inject_resource('utils.js'),
                inject_resource('blocks.js'),
                inject_resource('scores.js'),
                inject_resource('influences.js'),
                inject_resource('comparisons.js')
            ]))
    with doc.body:
        a(id='download-generator', style='hidden;')
        with div(cls='tile', id='blocks-tile'):
            with div(cls='fill'):
                with div(cls='plot'):
                    raw(block_map_div)
        with div(cls='tile', id='variable-comparison-tile'):
            with div(cls='header'):
                with div(cls='boxed'):
                    with span(cls='entry'):
                        label('Only selection')
                        input_(type='checkbox', checked='true', id='toggle-comparison-selected-blocks')
                    with span(cls='entry'):
                        label('Model')
                        input_(type='checkbox', checked='true', id='comparison-show-model')
                    with span(cls='entry'):
                        label('Cumulative')
                        input_(type='checkbox', checked='true', id='comparison-cumulative-model')
                    with span(cls='entry'):
                        label('Component')
                        input_(type='text', value='1', cls='short-text', id='comparison-model-component')
                    with span(cls='entry'):
                        input_(type='button', value='Download', id='download-comparison')
            with div(cls='fill'):
                with div(cls='plot'):
                    raw(variable_comparison_div)
        with div(cls='tile', id='scores-tile'):
            with div(cls='header'):
                with div(cls='boxed'):
                    with span(cls='entry'):
                        label('Lock')
                        input_(type='checkbox', id='lock-scores')
                    with span(cls='entry'):
                        label('Legend')
                        input_(type='checkbox', checked='true', id='legend-scores')
                    with span(cls='entry'):
                        label('Auto-scaling')
                        input_(type='checkbox', checked='true', id='toggle-axis-scaling')
                    with span(cls='entry'):
                        label('Main component')
                        input_(type='text', value='1', cls='short-text', id='component-main-field')
                    with span(cls='entry'):
                        label('Secondary component')
                        input_(type='text', value='2', cls='short-text', id='component-secondary-field')
                    with span(cls='entry'):
                        input_(type='button', value='Swap axes', id='swap-axes-button')
                        input_(type='button', value='Download', id='download-scores')
            with div(cls='fill'):
                with div(cls='plot'):
                    raw(score_traces_div)
        with div(cls='tile', id='influences-tile'):
            with div(cls='header'):
                with div(cls='boxed'):
                    with span(cls='entry'):
                        label('Lock')
                        input_(type='checkbox', id='lock-influences')
                    with span(cls='entry'):
                        label('Shown')
                        input_(type='text', value='20', cls='short-text', id='influences-shown-variables')
                    with span(cls='entry'):
                        label('Name filter')
                        input_(type='text', value='', cls='mid-text', id='influences-name-filter')
                    with span(cls='entry'):
                        input_(type='button', value='Download', id='download-influences')
            with div(cls='fill'):
                with div(cls='plot'):
                    raw(influence_bars_div)

    monitor.log('Writing')
    with open(outfile, 'w') as outfile_handle:
        outfile_handle.write(doc.render())
    if show:
        sp.run(['open', outfile], check=True)

def plotly_div_id(div):
    return re.search(r'div id="(?P<id>(\w|-)+)"', div).group('id')

def inject_js_reference(id, js_name):
    return "{} = document.getElementById('{}')".format(js_name, id)

def inject_plotly_reference(div, js_name):
    return inject_js_reference(plotly_div_id(div), js_name)

def inject_json(object, js_name):
    return "{} = {}".format(js_name, ExtendedEncoder().encode(object))

def inject_resource(file_name):
    return pr.resource_stream(__name__, file_name).read().decode('utf-8')

def inject_lines(lines_list):
    return '\n'.join(lines_list)

def inject_onload_scripts(scripts_list):
    return '\nwindow.addEventListener("load", function () {\n' + inject_lines(scripts_list) + '\n})\n'
