function updateAll() {
  updateScores()
  updateInfluences()
  updateComparisons()
  updateBlocks()
  resizeContainers()
}

function resizeContainers() {
  resizeContainer(block_map)
  resizeContainer(score_traces)
  resizeContainer(influence_bars)
  resizeContainer(variable_comparison_bars)
}

function resizeContainer(plot_object) {
  var container = plot_object.closest('.plot')
  Plotly.relayout(plot_object, {
    autosize: false,
    height: container.offsetHeight,
    width: container.offsetWidth
  })
}

function swapAxes() {
  var main = component_main_field.value
  var secondary = component_secondary_field.value
  component_main_field.value = secondary
  component_secondary_field.value = main
  updateAll()
}

function makeCsv(table) {
  var result_string = ''
  for (var i=0; i<table.length; i++) {
    var row = table[i]
    for (var j=0; j<row.length; j++) {
      var cell =  row[j] === null ? '' : row[j].toString()
      var next_token = cell
      if (cell.search(/("|,|\n)/g) >= 0) {
        var escaped = cell.replace(/"/g, '""');
        next_token = '"' + escaped + '"';
      }
      if (j < row.length-1) {
        next_token += ','
      }
      result_string += next_token
    }
    result_string += '\n'
  }
  return result_string
}

function make_download(table, file_name) {
  var output_text = makeCsv(table)
  download_generator.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(output_text))
  download_generator.setAttribute('download', file_name)
  download_generator.click()
}
