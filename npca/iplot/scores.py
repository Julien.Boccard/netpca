import numpy as np

from .utils import prepare_numbers

def make_score_traces(model, monitor, block_indices):
    import plotly.offline as po
    import plotly.graph_objs as go
    
    blocks = [block for block in model.dataset.get_blocks() if block.plot_set is not None]
    plot_observations = list(set([(block.plot_set, block.observations) for block in blocks]))
    block_dict = {key: [] for key in plot_observations}
    for block in blocks:
        block_dict[(block.plot_set, block.observations)].append(block)

    data = []
    component_indices = list(range(len(model.components)))
    for psi, (plot_set, observation) in enumerate(plot_observations):
        n_observations = observation.size()
        score_data = [{c_i: {} for c_i in component_indices} for _ in range(n_observations)]
        for c_i in component_indices:
            for b in block_dict[(plot_set, observation)]:
                monitor.state(f'Computing scores for {plot_set.label}, component {c_i}, block {b.key}')
                b_i = block_indices[b]
                scores = model.scores(c_i, b)
                for score_for_observation, score_data_for_observation in zip(scores, score_data):
                    score_data_for_observation[c_i][b_i] = prepare_numbers(score_for_observation)
        plot_color = 'rgb({},{},{})'.format(*plot_set.color)
        dummy_radius = (1+psi) / len(plot_observations)
        dummy_theta = 2 * np.pi * np.arange(0, 1, 1/n_observations)
        dummy_x = dummy_radius * np.cos(dummy_theta)
        dummy_y = dummy_radius * np.sin(dummy_theta)
        data.append(go.Scatter(
            name=plot_set.label,
            mode='markers',
            x=dummy_x,
            y=dummy_y,
            customdata=score_data,
            marker=dict(
                size=5,
                color=plot_color,
                symbol='circle',
                opacity=1
            )
        ))
    layout = go.Layout(
        title='Scores',
        dragmode='pan',
        hovermode='closest',
        margin=go.layout.Margin(l=50, r=50, b=50, pad=10),
        template='plotly_white',
        xaxis=dict(
            title='Main component',
            automargin=True
        ),
        yaxis=dict(
            title='Secondary component',
            automargin=True,
            scaleanchor = 'x'
        )
    )
    config = dict(
        modeBarButtons=[['pan2d', 'autoScale2d', 'toImage']],
        scrollZoom=True,
        toImageButtonOptions=dict(format='svg', filename='bpca-scores'),
        displaylogo=False
    )
    fig = go.Figure(data=data, layout=layout)
    div = po.plot(fig, include_plotlyjs=False, output_type='div', config=config)
    return div
