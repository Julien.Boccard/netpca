var comparison_variable_group_index = null
var comparison_variable_index = null

function updateComparisons() {
  if (comparison_variable_group_index && comparison_variable_index) {
    var model_cumulative = comparison_cumulative_model.checked
    var show_model = comparison_show_model.checked
    var component = parseInt(comparison_model_component.value) - 1
    var vi = comparison_variable_index
    var selected_block_indices = block_map.data[0].selectedpoints
    if (selected_block_indices && selected_block_indices.length > 0) {
      var selected_block_tags = selected_block_indices.map(i => block_map.data[0].customdata[i].index)
      var all_comparisons = variable_comparisons[comparison_variable_group_index]
      var block_indices = []
      var block_names = []
      var main_bases = []
      var main_heights = []
      var back_bases = []
      var back_heights = []
      var means = []
      var colors = []
      var hover = []
      var custom = []
      var model_stem_bases = []
      var model_stem_heights = []
      var model_main_bases = []
      var model_main_heights = []
      var model_back_bases = []
      var model_back_heights = []
      var model_hover = []
      var variable_name = 'N/A'
      var use_all_blocks = !toggle_comparison_selected_blocks.checked
      var current_index = 0
      var tags = []
      for (var tag in all_comparisons) {
        if (selected_block_tags.includes(tag) || use_all_blocks) {
          tags.push([tag, all_comparisons[tag].observation_order, all_comparisons[tag].block_name])
        }
      }
      function sortTags(a, b) {
        if (b[1] == a[1]) {
          return b[2] > a[2] ? 1 : (b[2] === a[2] ? 0 : -1)
        } else {
          return b[1] - a[1]
        }
      }
      tags = tags.sort(sortTags).map((a) => a[0])
      for (let tag of tags) {
        var c = all_comparisons[tag]
        variable_name = c.variable_names[vi]
        current_index += 1
        var hover_info = '<b>' + c.block_name +
          '</b> <i>' + c.plot_set_label + '</i>' +
          '<br>Mean: ' + c.mean[vi] +
          '<br>Median: ' + c.q50[vi] +
          '<br>IQR: ' + (c.q75[vi] - c.q25[vi])
        block_indices.push(current_index)
        block_names.push(c.block_name)
        colors.push(c.color)
        main_bases.push(c.q25[vi])
        main_heights.push(c.q75[vi] - c.q25[vi])
        back_bases.push(c.q05[vi])
        back_heights.push(c.q95[vi] - c.q05[vi])
        means.push(c.mean[vi])
        hover.push(hover_info)
        custom.push({
          block: c.block_name, mean: c.mean[vi], std: c.std[vi], q05: c.q05[vi], q25: c.q25[vi], q50: c.q50[vi], q75: c.q75[vi], q95: c.q95[vi]
        })
        model_stem_bases.push(c.model_center[vi])
        if (model_cumulative) {
          model_stem_heights.push(c.model_cumulative_mean[component][vi] - c.model_center[vi])
          model_main_bases.push(c.model_cumulative_q25[component][vi])
          model_main_heights.push(c.model_cumulative_q75[component][vi] - c.model_cumulative_q25[component][vi])
          model_back_bases.push(c.model_cumulative_q05[component][vi])
          model_back_heights.push(c.model_cumulative_q95[component][vi] - c.model_cumulative_q05[component][vi])
          model_hover.push('<b>Model contribution</b> <i>' + c.block_name + '</i><br>Components 1 to ' + (component+1))
        } else {
          model_stem_heights.push(c.model_mean[component][vi] - c.model_center[vi])
          model_main_bases.push(c.model_q25[component][vi])
          model_main_heights.push(c.model_q75[component][vi] - c.model_q25[component][vi])
          model_back_bases.push(c.model_q05[component][vi])
          model_back_heights.push(c.model_q95[component][vi] - c.model_q05[component][vi])
          model_hover.push('<b>Model contribution</b> <i>' + c.block_name + '</i><br>Component ' + (component+1))
        }
      }

      var data_bar_offset = show_model ? -0.5 : -0.4
      var data_bar_width = show_model ? 0.6 : 0.8
      data_update = [{ // full data spread
        type: 'bar',
        base: back_bases,
        x: block_indices,
        y: back_heights,
        width: data_bar_width,
        offset: data_bar_offset,
        hovertext: hover,
        hoverinfo: 'text',
        customdata: custom,
        marker: {
          opacity: 0.25,
          color: colors
        }
      }, {
        type: 'bar', // interquartile
        base: main_bases,
        y: main_heights,
        x: block_indices,
        width: data_bar_width,
        offset: data_bar_offset,
        hoverinfo: 'skip',
        marker: {
          color: colors,
        }
      }]
      if (show_model) {
        data_update.push(...[
          { // model full spread
            type: 'bar',
            base: model_back_bases,
            y: model_back_heights,
            x: block_indices,
            width: 0.2,
            offset: +0.15,
            hovertext: model_hover,
            hoverinfo: 'text',
            marker: {
              color: 'rgb(200,200,200)',
            }
          }, { // model interquartile
            type: 'bar',
            base: model_main_bases,
            y: model_main_heights,
            x: block_indices,
            width: 0.2,
            offset: +0.15,
            hoverinfo: 'skip',
            marker: {
              color: 'rgb(150,150,150)',
            }
          }, { // model stem
            type: 'bar',
            base: model_stem_bases,
            y: model_stem_heights,
            x: block_indices,
            width: 0.05,
            offset: +0.225,
            hoverinfo: 'skip',
            marker: {
              color: 'rgb(0,0,0)',
            }
          }
        ])
      }
      var old_layout = variable_comparison_bars.layout
      var old_config = variable_comparison_bars.config
      var title = 'Variable comparison: ' + variable_name
      if (model_cumulative) {
        title += '<br>Components 1 - ' + (component + 1)
      } else {
        title += '<br>Component ' + (component + 1)
      }
      old_layout.title = title
      old_layout.xaxis.tickvals = block_indices
      old_layout.xaxis.ticktext = block_names
      Plotly.react(variable_comparison_bars, data_update, old_layout, old_config)
    }
  }
}

function downloadComparison() {
  var table = [[variable_comparison_bars.layout.title.text], ['Block', 'Mean', 'Std', 'P05%', 'P25%', 'Median', 'P75%', 'P95%']]
  var cdata = variable_comparison_bars.data[0].customdata
  if (cdata) {
    for (let c of cdata) {
      table.push([c.block, c.mean, c.std, c.q05, c.q25, c.q50, c.q75, c.q95])
    }
    make_download(table, 'bpca-variable-comparison.csv')
  }
}
