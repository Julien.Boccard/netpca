function updateInfluences() {
  if (lock_influences.checked) {
    return;
  }
  
  var n_vars = parseInt(influences_shown_variables.value)
  var component = parseInt(component_main_field.value) - 1

  var selected_block_indices = block_map.data[0].selectedpoints
  if (selected_block_indices && selected_block_indices.length > 0) {
    var selected_blocks = selected_block_indices.map(i => block_map.data[0].customdata[i].index)
    var selected_variable_group = null
    var selected_variable_group_index = null
    for (var tag in variable_groups) {
      var group_info = variable_groups[tag]
      if (group_info.blocks.includes(selected_blocks[0])) {
        selected_variable_group_index = tag
        selected_variable_group = group_info
        break
      }
    }
    selected_blocks = selected_blocks.filter(b => selected_variable_group.blocks.includes(b))
    var loading_is_positive = selected_variable_group.loadings[component].map(l => l>=0)

    var passes_regex = selected_variable_group.variable_names.map(n => true)
    if (influences_name_filter.value.length > 0) {
      var re = RegExp(influences_name_filter.value)
      passes_regex = selected_variable_group.variable_names.map(n => re.test(n.toLowerCase().replace(/\W/g, '')))
    }

    var influence_over_selected_blocks = selected_variable_group.loadings[component].map(x => 0)
    for (let sel_block of selected_blocks) {
      for (var i=0; i<influence_over_selected_blocks.length; i++) {
        influence_over_selected_blocks[i] += influences[component][sel_block].infs[i]
      }
    }
    var influence_indices = influence_over_selected_blocks.map((inf, i) => [inf, i])
    var max_influence = Math.max(...influence_over_selected_blocks)

    function sortIndices(a, b) {
      var inf_a = a[0]
      var inf_b = b[0]
      var ind_a = a[1]
      var ind_b = b[1]
      if (passes_regex[ind_b] && !passes_regex[ind_a]) {
        return +1
      } else if (passes_regex[ind_a] && !passes_regex[ind_b]) {
        return -1
      } else {
        return inf_b - inf_a
      }
    }
    var positive_indices = influence_indices.filter(
      inf_i => loading_is_positive[inf_i[1]]
    ).sort(sortIndices).slice(0, n_vars).map(inf_i => inf_i[1])
    var negative_indices = influence_indices.filter(
      inf_i => !loading_is_positive[inf_i[1]]
    ).sort(sortIndices).slice(0, n_vars).map(inf_i => inf_i[1])

    var x_range = positive_indices.map((v, i) => i)
    var data_update = []
    function format_influence(inf_value) {
      var abs_value = Math.abs(inf_value)
      if (abs_value < 1e-4) {
        return (1e6 * inf_value) + ' (ppm)'
      } else if (abs_value < 1e-2) {
        return (1e3 * inf_value) + ' (\u2030)'
      } else {
        return  (1e2 * inf_value) + ' (%)'
      }
    }
    function generate_trace(trace_block, trace_is_positive) {
      var indices = trace_is_positive ? positive_indices : negative_indices
      var all_infs = influences[component][trace_block].infs
      var infs = indices.map(i => all_infs[i])
      function make_info(index) {
        var base = selected_variable_group.block_names[trace_block] +
              ': ' + selected_variable_group.variable_names[index] +
              '<br>' + format_influence(all_infs[index])
        for (var key in selected_variable_group.metadata) {
          base += '<br> - ' + key + ': ' + selected_variable_group.metadata[key][index]
        }
        return base
      }
      var variable_info = indices.map(make_info)
      var custom_info = indices.map(i => ({
        variable_group_index: selected_variable_group_index,
        variable_index: i,
        block: selected_variable_group.block_names[trace_block],
        variable: selected_variable_group.variable_names[i],
        loading: selected_variable_group.loadings[component][i],
        influence: all_infs[i],
        sign: trace_is_positive ? 'Positive' : 'Negative'
      }))
      var xaxis_label =  trace_is_positive ? 'x' : 'x2'
      var yaxis_label =  trace_is_positive ? 'y' : 'y2'
      return {
        type: 'bar',
        x: x_range,
        y: infs,
        xaxis: xaxis_label,
        yaxis: yaxis_label,
        customdata: custom_info,
        hovertext: variable_info,
        hoverinfo: 'text',
        marker: {
          color: influences[component][trace_block].color,
          opacity: (selected_blocks.includes(trace_block) ? 1 : 0.3)
        }
      }
    }

    var trace_blocks = selected_variable_group.blocks.sort(
      (a, b) => (selected_blocks.includes(b) ? 1 : 0) - (selected_blocks.includes(a) ? 1 : 0)
    )
    for (var i=0; i<trace_blocks.length; i++) {
      data_update.push(generate_trace(trace_blocks[i], true))
      data_update.push(generate_trace(trace_blocks[i], false))
    }
    var old_layout = influence_bars.layout
    var old_config = influence_bars.config
    old_layout.title = 'Component ' + (component+1) + ' influences: ' + selected_variable_group.name
    old_layout.yaxis.autorange = false
    old_layout.yaxis2.autorange = false
    old_layout.yaxis.range = [-0.25 * max_influence, 1.25 * max_influence]
    old_layout.yaxis2.range = [-0.25 * max_influence, 1.25 * max_influence]
    Plotly.react(influence_bars, data_update, old_layout, old_config)
  }
}

function influenceBarCallback(data) {
  comparison_variable_group_index = data.points[0].customdata.variable_group_index
  comparison_variable_index = data.points[0].customdata.variable_index
  updateComparisons()
}

function downloadCurrentInfluences() {
  var header = [[influence_bars.layout.title.text], ['Variable', 'Loading', 'Sign', 'Block', 'Influence']]
  var pos_tables = influence_bars.data[0].customdata.map(i => [])
  var neg_tables = influence_bars.data[0].customdata.map(i => [])
  for (let variables of influence_bars.data) {
    for (var i=0; i<variables.customdata.length; i++) {
      var d = variables.customdata[i]
      var t = d.loading >= 0 ? pos_tables : neg_tables
      t[i].push([d.variable, d.loading, d.sign, d.block, d.influence])
    }
  }
  table = header.concat(...pos_tables).concat(...neg_tables)
  make_download(table, 'bpca-influences.csv')
}
