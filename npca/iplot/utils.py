import json
import decimal as dc
import numpy as np

def get_block_color(block, default=(150, 150, 150)):
    """
    Returns a plotly-style specification of hte block color from its PlotSet object,
    of <default> if the PlotSet is None
    """
    color = default
    if block.plot_set is not None:
        color = block.plot_set.color
    return 'rgb({},{},{})'.format(*color)

def prepare_numbers(obj, precision=6, threshold=1e-9):
    """
    Prepares <obj> to be inserted in a plotly graphic
    """
    if isinstance(obj, np.ndarray):
        return to_decimal(obj, precision, threshold)
    else:
        return obj
    
def to_decimal(values, precision, threshold):
    offsets = (np.ceil(np.log10(np.maximum(threshold, np.abs(values)))) - precision)
    rounded = np.floor(values / np.power(10.0, offsets))
    return [dc.Decimal(int(r)) * (dc.Decimal(10)**int(o)) for r, o in zip(rounded, offsets)]

class ExtendedEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, dc.Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)
