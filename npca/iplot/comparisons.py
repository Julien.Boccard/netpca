import numpy as np

from .utils import prepare_numbers, get_block_color

def make_comparison_bars(model, monitor, block_indices, variable_group_indices, selected_numpy_variable_indices):
    import plotly.offline as po
    import plotly.graph_objs as go
    
    variable_groups = model.dataset.get_groups(observations=False, variables=True)
    blocks = model.dataset.get_blocks()

    blocks_by_variable_group = {v: [] for v in variable_groups}
    for b in blocks:
        blocks_by_variable_group[b.variables].append(b)

    variable_comparison_dict = {}
    for v in variable_groups:
        by_block = {}
        variable_comparison_dict[variable_group_indices[v]] = by_block
        for b in blocks_by_variable_group[v]:
            monitor.state(f'Variable group {v.label} - block {b.key}')
            for_block = {}
            by_block[block_indices[b]] = for_block
            raw_data = b.data * b.scale + b.center

            model_data = [component.block_reconstruct(b) for component in model.components]
            model_cumulative_data = [model_data[0]]
            for m_d in model_data[1:]:
                model_cumulative_data.append(model_cumulative_data[-1] + m_d)
            model_data = [m_d * b.scale + b.center for m_d in model_data]
            model_cumulative_data = [m_d * b.scale + b.center for m_d in model_cumulative_data]

            raw_data = raw_data[:, selected_numpy_variable_indices[v]]
            model_data = [d[:, selected_numpy_variable_indices[v]] for d in model_data]
            model_cumulative_data = [d[:, selected_numpy_variable_indices[v]] for d in model_cumulative_data]

            plot_set_label = '?'
            if b.plot_set is not None:
                plot_set_label = b.plot_set.label
            pn = prepare_numbers
            for_block['variable_names'] = [v.ids[i] for i in selected_numpy_variable_indices[v]]
            for_block['observation_order'] = b.observations.order
            for_block['block_name'] = b.key
            for_block['plot_set_label'] = plot_set_label
            for_block['color'] = get_block_color(b)
            for_block['mean'] = pn(np.mean(raw_data, axis=0))
            for_block['std'] = pn(np.std(raw_data, axis=0))
            for_block['q05'] = pn(np.quantile(raw_data, 0.05, axis=0))
            for_block['q25'] = pn(np.quantile(raw_data, 0.25, axis=0))
            for_block['q50'] = pn(np.quantile(raw_data, 0.50, axis=0))
            for_block['q75'] = pn(np.quantile(raw_data, 0.75, axis=0))
            for_block['q95'] = pn(np.quantile(raw_data, 0.95, axis=0))
            for_block['model_center'] = pn(b.center[0, selected_numpy_variable_indices[v]])
            for_block['model_mean'] = [pn(np.mean(data, axis=0)) for data in model_data]
            for_block['model_q05'] = [pn(np.quantile(data, 0.05, axis=0)) for data in model_data]
            for_block['model_q25'] = [pn(np.quantile(data, 0.25, axis=0)) for data in model_data]
            for_block['model_q50'] = [pn(np.quantile(data, 0.50, axis=0)) for data in model_data]
            for_block['model_q75'] = [pn(np.quantile(data, 0.75, axis=0)) for data in model_data]
            for_block['model_q95'] = [pn(np.quantile(data, 0.95, axis=0)) for data in model_data]
            for_block['model_cumulative_mean'] = [pn(np.mean(data, axis=0)) for data in model_cumulative_data]
            for_block['model_cumulative_q05'] = [pn(np.quantile(data, 0.05, axis=0)) for data in model_cumulative_data]
            for_block['model_cumulative_q25'] = [pn(np.quantile(data, 0.25, axis=0)) for data in model_cumulative_data]
            for_block['model_cumulative_q50'] = [pn(np.quantile(data, 0.50, axis=0)) for data in model_cumulative_data]
            for_block['model_cumulative_q75'] = [pn(np.quantile(data, 0.75, axis=0)) for data in model_cumulative_data]
            for_block['model_cumulative_q95'] = [pn(np.quantile(data, 0.95, axis=0)) for data in model_cumulative_data]
    
    data = [
        go.Box(
            x=[],
            y=[]
        )
    ]
    layout = go.Layout(
        barmode='overlay',
        showlegend=False,
        title='Variable comparison',
        dragmode='pan',
        hovermode='closest',
        margin=go.layout.Margin(l=50, r=50, b=50, pad=10),
        template='plotly_white',
        xaxis=dict(
            type='category',
            tickmode='array',
            automargin=True
        ),
        yaxis=dict(
            rangemode='tozero',
            automargin=True,
        )
    )
    config = dict(
        modeBarButtons=[['pan2d', 'autoScale2d', 'toImage']],
        toImageButtonOptions=dict(format='svg', filename='bpca-variable-comparison'),
        scrollZoom=True,
        displaylogo=False
    )
    fig = go.Figure(data=data, layout=layout)
    div = po.plot(fig, include_plotlyjs=False, output_type='div', config=config)
    return div, variable_comparison_dict
