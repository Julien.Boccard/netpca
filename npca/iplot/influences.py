import numpy as np

from .utils import prepare_numbers, get_block_color

def make_influence_bars(model, monitor, block_indices, min_variables_per_group):
    import plotly.offline as po
    import plotly.graph_objs as go
    
    variable_groups = model.dataset.get_groups(observations=False, variables=True)
    blocks = model.dataset.get_blocks()
    component_indices = range(len(model.components))

    def retrieve_influences(c_i):
        monitor.state(f'Computing influences for component {c_i}')
        return model.variable_influences(c_i)
    influences_full = {c_i: retrieve_influences(c_i) for c_i in component_indices}

    monitor.state('Sorting')
    selected_numpy_variable_indices = {
        g: np.unique([
            np.argsort(-influences_full[c_i][b])[:min_variables_per_group]
            for c_i in component_indices for b in blocks if b.variables == g
        ])
        for g in variable_groups
    }
    
    variable_group_index = 0
    variable_group_indices = {}
    for variable_group in variable_groups:
        variable_group_indices[variable_group] = 'variable-group-{}'.format(variable_group_index)
        variable_group_index += 1

    monitor.state('Filtering')
    def filter_var(group, items):
        return [items[i] for i in selected_numpy_variable_indices[group]]

    influences_dict = {
        c_i: {
            block_indices[b]: {
                'infs': prepare_numbers(filter_var(b.variables, infs)),
                'color': get_block_color(b)
            }
            for b, infs in influences_full[c_i].items()
        }
        for c_i in component_indices
    }
    block_influences_dict = {
        c_i: {
            block_indices[b]: np.sum(infs)
            for b, infs in influences_full[c_i].items()
        }
        for c_i in component_indices
    }
    variable_groups_dict = {variable_group_indices[g]: {
        'name': g.label,
        'variable_names': filter_var(g, g.ids),
        'metadata': {key: filter_var(g, items) for key, items in g.metadata.items()},
        'loadings': {
            c_i: prepare_numbers(filter_var(g, model.components[c_i].loadings[g]))
            for c_i in component_indices
        },
        'blocks': [],
        'block_names' : {}
    } for g in variable_groups}
    for b in blocks:
        variable_groups_dict[variable_group_indices[b.variables]]['blocks'].append(block_indices[b])
        variable_groups_dict[variable_group_indices[b.variables]]['block_names'][block_indices[b]] = b.key

    data = [
        go.Bar(
            x=[],
            y=[],
            xaxis='x',
            yaxis='y'
        ),
        go.Bar(
            x=[],
            y=[],
            xaxis='x2',
            yaxis='y2'
        )
    ]
    layout = go.Layout(
        barmode='relative',
        showlegend=False,
        title='Influences',
        dragmode='pan',
        hovermode='closest',
        margin=go.layout.Margin(l=75, r=50, b=20, pad=10),
        template='plotly_white',
        xaxis=dict(
            domain=[0, 1],
            showticklabels=False
        ),
        yaxis=dict(
            domain=[0.51, 1],
            automargin=True,
            title='Positive'
        ),
        xaxis2=dict(
            domain=[0, 1],
            showticklabels=False
        ),
        yaxis2=dict(
            domain=[0, 0.49],
            automargin=True,
            title='Negative'
        )
    )
    config = dict(
        modeBarButtons=[['pan2d', 'autoScale2d', 'toImage']],
        toImageButtonOptions=dict(format='svg', filename='bpca-influences'),
        scrollZoom=True,
        displaylogo=False
    )
    fig = go.Figure(data=data, layout=layout)
    div = po.plot(fig, include_plotlyjs=False, output_type='div', config=config)
    return div, influences_dict, block_influences_dict, variable_groups_dict, variable_group_indices, selected_numpy_variable_indices
