function updateScores() {
  if (lock_scores.checked) {
    return;
  }

  var selectedblocks = block_map.data[0].selectedpoints
  if (!selectedblocks) {
    selectedblocks = []
  }
  var component_x = parseInt(component_main_field.value) - 1
  var component_y = parseInt(component_secondary_field.value) - 1
  var selected_block_indices = selectedblocks.map(i => block_map.data[0].customdata[i].index)
  var trace_indices = score_traces.data.map((value, i) => i)
  var data_updates = score_traces.data.map(computeScoreDataUpdate(selected_block_indices, component_x, component_y))
  var layout_update = computeScoreLayoutUpdate(data_updates, component_x, component_y)
  if (toggle_axis_rescaling_checkbox.checked) {
    Plotly.relayout(score_traces, layout_update)
  }
  component_influence_x = 0
  component_influence_y = 0
  for (let block_index of selected_block_indices) {
      if (block_index in block_influences[component_x]) {
        component_influence_x += block_influences[component_x][block_index]
      }
      if (block_index in block_influences[component_y]) {
        component_influence_y += block_influences[component_y][block_index]
      }
  }
  var title_update = {
    'showlegend': legend_scores.checked,
    'xaxis.title': 'Component ' + (component_x + 1) + ' (' + (100 * component_influence_x).toFixed(2) + '%)',
    'yaxis.title': 'Component ' + (component_y + 1) + ' (' + (100 * component_influence_y).toFixed(2) + '%)',
    'title': 'Scores (' + (component_x + 1) + ' vs ' + (component_y + 1) + ')'
  }
  Plotly.relayout(score_traces, title_update)
  Plotly.animate(score_traces,
    { layout: {}, data: data_updates, traces: trace_indices },
    { transition: { duration: 500, easing: 'cubic-in-out' }, frame: { duration: 500 } }
  )
}

function computeScoreLayoutUpdate(data_updates, component_x, component_y) {
  min_x = 0
  max_x = 0
  min_y = 0
  max_y = 0
  for (let update of data_updates) {
    min_x = Math.min(min_x, Math.min(...update.x))
    max_x = Math.max(max_x, Math.max(...update.x))
    min_y = Math.min(min_y, Math.min(...update.y))
    max_y = Math.max(max_y, Math.max(...update.y))
  }
  center_x = 0.5 * max_x + 0.5 * min_x
  center_y = 0.5 * max_y + 0.5 * min_y
  width_x = 0.5 * max_x - 0.5 * min_x
  width_y = 0.5 * max_y - 0.5 * min_y
  return {
    'xaxis.range': [center_x - 1.15 * width_x, center_x + 1.15 * width_x],
    'yaxis.range': [center_y - 1.15 * width_y, center_y + 1.15 * width_y]
  }
}

function computeScoreDataUpdate(selected_block_indices, component_x, component_y) {
  return function(trace) {
    var score_x = []
    var score_y = []
    var group_data = { anySelected: false }
    for (var i=0; i<trace.customdata.length; i++) {
      score_x.push(computeSingleScore(component_x, trace.customdata[i], selected_block_indices, group_data))
      score_y.push(computeSingleScore(component_y, trace.customdata[i], selected_block_indices, group_data))
    }
    return {
      x: score_x,
      y: score_y,
      'marker.opacity': group_data.anySelected ? 1.0 : 0.0
    }
  }
}

function computeSingleScore(component, score_data, selected_block_indices, group_data) {
  var result = 0
  var n_blocks = 0
  for (var block_id in score_data[component]) {
    if (selected_block_indices.includes(block_id)) {
      group_data.anySelected = true
      result += score_data[component][block_id]
      n_blocks += 1
    }
  }
  if (n_blocks > 0) {
    result /= n_blocks
  }
  return result
}

function downloadCurrentScores() {
  var table = [[score_traces.layout.title.text], ['Group', 'Color', 'X score', 'Y score']]
  for (let group of score_traces.data) {
    for (var i=0; i<group.x.length; i++) {
      if (group.marker.opacity > 0.5) {
        table.push([group.name, group.marker.color, group.x[i], group.y[i]])
      }
    }
  }
  make_download(table, 'bpca-scores.csv')
}
