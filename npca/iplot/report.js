block_map.on('plotly_selected', blockSelectionCallback)
block_map.on('plotly_click', blockClickCallback)
block_map.on('plotly_relayout', blockLayoutCallback)
influence_bars.on('plotly_click', influenceBarCallback)

component_main_field.onchange = updateAll
component_secondary_field.onchange = updateAll
toggle_axis_rescaling_checkbox.onchange = updateScores
toggle_comparison_selected_blocks.onchange = updateComparisons
lock_influences.onchange = updateInfluences
lock_scores.onchange = updateScores
legend_scores.onchange = updateScores
influences_shown_variables.onchange = updateInfluences
influences_name_filter.onchange = updateInfluences
comparison_cumulative_model.onchange = updateComparisons
comparison_show_model.onchange = updateComparisons
comparison_model_component.onchange = updateComparisons
download_scores.onclick = downloadCurrentScores
download_influences.onclick = downloadCurrentInfluences
download_comparison.onclick = downloadComparison
swap_axes_button.onclick = swapAxes
window.addEventListener('resize', function () {
  resizeContainers()
})

updateBlocks()
resizeContainers()
