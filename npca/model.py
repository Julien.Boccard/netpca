import numpy as np

from .iplot import report

class Model:
    """
    Container for a dataset and a decomposition
    """
    def __init__(s, dataset, components):
        s.dataset = dataset
        s.components = components

    def scores(s, index, block):
        """
        Computes observation scores
        <index> of the component
        <block> on which they are computed
        """
        return np.dot(block.data, s.components[index].loadings[block.variables]) / np.power(block.variables.size(), 0.5)
    
    def loadings(s, index, group):
        """
        Same as scores, for model loadings
        """
        return s.components[index].loadings[group]
    
    def weight(s, index, block):
        """
        Same as scores, for model weights
        """
        return s.components[index].weights[block]

    def explained_variances_per_variable(s, component_index, block):
        """
        For the <component_index>-th component in this model
        computes an array of variance explained per variable
        (change of variance when suppressing the variable in the block)
        on the given <block>
        """
        comp = s.components[component_index]
        proj_contribution = comp.weights[block] * comp.loadings[block.variables] * np.dot(comp.loadings[block.observations], block.data)
        overlap_contribution = 0
        for overlap_comp in s.components[:component_index]:
            obs_dot = np.dot(comp.loadings[block.observations], overlap_comp.loadings[block.observations])
            weights = comp.weights[block] * overlap_comp.weights[block]
            vars = comp.loadings[block.variables] * overlap_comp.loadings[block.variables]
            overlap_contribution += obs_dot * weights * vars
        return proj_contribution - overlap_contribution

    def loading_orthogonality_matrices(s):
        return {
            group: np.array(
                [[np.dot(c1.loadings[group], c2.loadings[group]) for c1 in s.components] for c2 in s.components]
            )
            for group in s.dataset.get_groups()
        }

    def variable_influences(s, component_index, unweighted=False, per_variable=False):
        """
        For the <component_index>-th component in this model
        returns a dict of all the blocks of the underlying dataset
        to their relative influences (explained variances per variable vs total dataset variance)
        given as arrays
        If <per_variable> is True, the influences are normalized to the total variance of each variable,
        otherwise to the total of the dataset
        If <unweighted> is True, block charges are ignored when computing the variance
        """
        blocks = s.dataset.get_blocks()
        total_var = s.dataset.variance(unweighted, per_variable)
        result = {}
        for block in blocks:
            weighted_loading = s.components[component_index].weighted_variable_loading(block)
            loading_terms = weighted_loading
            projection_terms = weighted_loading - np.dot(s.components[component_index].loadings[block.observations], block.data)
            orthogonal_terms = np.sum([
                s.components[previous_component].weighted_variable_loading(block) * np.dot(
                    s.components[previous_component].loadings[block.observations],
                    s.components[component_index].loadings[block.observations]
                )
                for previous_component in range(component_index)
            ], axis=0)
            charge_term = 1 if unweighted else block.charge**2
            normalization = total_var
            if per_variable:
                normalization = total_var[block.variables]
            result[block] = charge_term * weighted_loading * (loading_terms - 2 * projection_terms - 2 * orthogonal_terms) / normalization
        return result

    def block_influences(s, component_index, unweighted=False, per_variable=False):
        variable_influences = s.variable_influences(component_index, unweighted, per_variable)
        return {block: np.sum(influences) for block, influences in variable_influences.items()}

    def report(s, out_file, **report_args):
        """
        Generates a report of this model into <out_file> in HTML format
        For details on <report_args> see iplot.report.report
        """
        report.report(s, out_file, **report_args)

class Component:
    """
    Contains a single component of a model, with
    a dict of <loadings> for each group and
    a dict of <weights> for each block
    """
    def __init__(s, loadings, weights):
        s.loadings = loadings
        s.weights = weights

    def block_reconstruct(s, block):
        """
        Gives the contribution of this component to the reconstruction of <block>
        """
        return s.weights[block] * np.multiply.outer(s.loadings[block.observations], s.loadings[block.variables])
    
    def weighted_variable_loading(s, block):
        """
        Gives the best fit for this block of its projection on observations
        """
        return s.weights[block] * s.loadings[block.variables]
