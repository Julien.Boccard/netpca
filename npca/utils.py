import re
import numpy as np

# Exceptions
class DataFormatException(Exception):
    pass

class InvalidIndexException(Exception):
    pass

class InvalidGroupException(Exception):
    pass

class InvalidBlockException(Exception):
    pass

def to_regex(exp):
    """
    If <exp> is a string builds an exact match regex from it, if not returns <exp>
    """
    res = exp
    if isinstance(res, str):
        res = re.compile(r'\A' + re.escape(res) + r'\Z')
    return res

def normalize(v):
    """
    Returns a L2-normalized copy of <v>
    """
    return v / np.linalg.norm(v)

def inspect(obj, level=None, max_level=0):
    """
    Returns a string with a tree description of <obj>
    with at most <max_level> depth
    <level> is used internally for recursion
    """
    if level is not None:
        tabs = ''.join(['|    ' for i in range(level)])
        type_string = '[' + type(obj).__name__ + ']'
        if hasattr(obj, 'name'):
            name_string = getattr(obj, 'name') + ' '
        else:
            name_string = ''
        result = name_string + type_string + '\n'
        has_children = False
        if (max_level == 0 or level <= max_level):
            pair_iter = None
            if hasattr(obj, '__dict__'):
                pair_iter = obj.__dict__.items()
            elif isinstance(obj, list):
                pair_iter = enumerate(obj)
            elif isinstance(obj, dict):
                pair_iter = obj.items()
            if pair_iter:
                has_children = True
                result += tabs + '|\n'
                for key, subobj in pair_iter:
                    subobj_string, sub_has_children = inspect(subobj, level + 1, max_level)
                    node_symbol = '#' if sub_has_children else '>'
                    result += tabs + '|----{} {}: '.format(node_symbol, key) + subobj_string
                result += tabs + '‾\n'
        return result, has_children
    else:
        result, _ = inspect(obj, level=0, max_level=max_level)
        return result

