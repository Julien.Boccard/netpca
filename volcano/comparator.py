import numpy as np
import scipy.stats as st

from ..io import filters

from .plot import plot_static, plot_interactive

def ttest(*args, **kwargs):
    return Comparator(*args, **kwargs)

class Comparator:
    def __init__(s, intensity_threshold, fold_threshold, min_pvalue, min_log_fold):
        s.intensity_threshold = intensity_threshold
        s.fold_threshold = fold_threshold
        s.min_pvalue = min_pvalue
        s.min_log_fold = min_log_fold

    def run(s, datatable, group_property, control_group, exclude_groups=None):
        if exclude_groups is None:
            exclude_groups = []
        return ComparisonSet(datatable, group_property, s, control_group, exclude_groups)

class ComparisonSet:
    def __init__(s, datatable, group_property, comparator, control_group, exclude_groups):
        group_names = datatable.observation_metadata[group_property].select().values
        s.comparison_groups = list(set(group_names) - set([control_group]) - set(exclude_groups))
        s.comparisons = [
            Comparison(datatable, group_property, control_group, target_group, comparator)
            for target_group in s.comparison_groups
        ]

    def __iter__(s):
        return s.comparisons.__iter__()

    def __getitem__(s, index):
        try:
            return s.comparisons[index]
        except TypeError:
            for c in s.comparisons:
                if c.target_group == index:
                    return c
            raise TypeError(f'Invalid index {index}')

    def __len__(s):
        return len(s.comparisons)
    
    def add_to_source(s, *args, **kwargs):
        for comparison in s:
            comparison.add_to_source(*args, **kwargs)

class Comparison:
    def __init__(s, datatable, group_property, control_group, target_group, comparator):
        s.target_group = target_group
        s.control_group = control_group
        s.datatable = datatable
        s.comparator = comparator

        control_data = datatable.select(
            observation_mask=datatable.observation_mask(group_property, filters.valid_id(exact=control_group))
        ).data
        group_data = datatable.select(
            observation_mask=datatable.observation_mask(group_property, filters.valid_id(exact=target_group))
        ).data

        control_means = np.mean(control_data, axis=0)
        group_means = np.mean(group_data, axis=0)
        if comparator.fold_threshold is None:
            s.folds = np.maximum(
                group_means, comparator.intensity_threshold
            ) / np.maximum(
                control_means, comparator.intensity_threshold
            )
            s.log2_folds = np.log2(s.folds)
        else:
            log_limit = np.log(2) * comparator.fold_threshold
            pow_s = np.power(np.maximum(control_means, comparator.intensity_threshold), 2 / log_limit)
            pow_e = np.power(np.maximum(group_means, comparator.intensity_threshold), 2 / log_limit)
            s.log2_folds = log_limit * (pow_e - pow_s) / (pow_e + pow_s)
            s.folds = np.power(2, s.log2_folds)
        
        with np.errstate(invalid='ignore'):
            s.tstats, s.pvalues = st.ttest_ind(control_data, group_data, axis=0, equal_var=False)
        s.log10_pvalues = - np.log10(s.pvalues)
        for i in range(len(s.pvalues)):
            if np.isnan(s.pvalues[i]):
                s.pvalues[i] = 1.0
        
        s.scores = np.sqrt(
            np.power(s.log2_folds / comparator.min_log_fold, 2) +
            np.power(np.log2(s.pvalues) / np.log2(comparator.min_pvalue), 2)
        )
        s.selection = np.logical_and(
            np.abs(s.log2_folds) > comparator.min_log_fold,
            s.pvalues < comparator.min_pvalue
        )
    
    def add_to_source(s, key_prefix, format_string='{}: {} vs {} ({})'):
        s.datatable.map_variable_metadata(
            new_key=format_string.format(key_prefix, s.target_group, s.control_group, 'p-Value'),
            mapping_function=lambda i: s.pvalues[i]
        )
        s.datatable.map_variable_metadata(
            new_key=format_string.format(key_prefix, s.target_group, s.control_group, 'Log2-Fold'),
            mapping_function=lambda i: s.log2_folds[i]
        )
        s.datatable.map_variable_metadata(
            new_key=format_string.format(key_prefix, s.target_group, s.control_group, 'Fold'),
            mapping_function=lambda i: s.folds[i]
        )
        s.datatable.map_variable_metadata(
            new_key=format_string.format(key_prefix, s.target_group, s.control_group, 'Score'),
            mapping_function=lambda i: s.scores[i]
        )
    
    def plot(s, interactive=True, **kwargs):
        if interactive:
            plot_interactive(s, **kwargs)
        else:
            plot_static(s, **kwargs)