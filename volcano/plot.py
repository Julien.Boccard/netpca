import numpy as np

def plot_static(s, view_pvalue, view_log_fold, **figure_kwargs):
    import matplotlib.pyplot as pl

    selection_pvalues = s.pvalues[s.selection]
    selection_log_folds = s.log2_folds[s.selection]
    comparator = s.comparator
    pl.figure(**figure_kwargs)
    pl.plot(s.log2_folds, s.pvalues,
        linestyle='none', marker='o', markersize=4, markerfacecolor=(1,1,1), markeredgecolor=(0,0,0))
    pl.plot(selection_log_folds, selection_pvalues,
        linestyle='none', marker='o', markersize=4, markerfacecolor=(0.5,0.5,0.5), markeredgecolor=(0,0,0))
    pl.plot([-comparator.min_log_fold, -comparator.min_log_fold], [-1, 2],
        linestyle='--', color=(0.8,0,0), linewidth=1)
    pl.plot([+comparator.min_log_fold, +comparator.min_log_fold], [-1, 2],
        linestyle='--', color=(0.8,0,0), linewidth=1)
    pl.plot([-2*view_log_fold, 2*view_log_fold], [comparator.min_pvalue, comparator.min_pvalue],
        linestyle='--', color=(0.8,0,0), linewidth=1)
    pl.ylim([2, view_pvalue])
    pl.xlim([-view_log_fold, +view_log_fold])
    pl.yscale('log')
    pl.ylabel('$p$ value')
    pl.xlabel(r'$\log_2$ fold change')
    pl.title(f'Volcano for {s.target_group} vs. {s.control_group}')

def plot_interactive(s, view_pvalue, view_log_fold, variable_name_property, backend='canvas', **bokeh_args):
    import bokeh.models as bmd
    import bokeh.plotting as bpt
    import bokeh.io as bio
    comparator = s.comparator
    
    hover_tool = bmd.HoverTool(
        tooltips=[
            ('p-Value', '@pvalues'),
            ('log2-fold', '@logfolds'),
            ('Variable', '@variables')
        ]
    )
    pan_tool = bmd.PanTool()
    zoom_tool = bmd.WheelZoomTool()
    fig = bpt.figure(
        tools=[hover_tool, pan_tool, zoom_tool, bmd.SaveTool(), bmd.BoxZoomTool(), bmd.ResetTool()],
        x_range=bmd.Range1d(-view_log_fold, +view_log_fold),
        y_range=bmd.Range1d(2, view_pvalue), y_axis_type='log',
        x_axis_label='log-2 fold change', y_axis_label='p-value',
        output_backend=backend,
        title=(
            f'Volcano for {s.target_group} vs. {s.control_group}'
        )
    )
    fig.toolbar.active_drag = pan_tool
    fig.toolbar.active_inspect = hover_tool
    fig.toolbar.active_scroll = zoom_tool
    
    variable_names = s.datatable.variable_metadata[variable_name_property].values
    variable_mask = s.datatable.variable_metadata[variable_name_property].mask
    data_source = bmd.ColumnDataSource({
        'pvalues': s.pvalues,
        'logfolds': s.log2_folds,
        'variables': variable_names,
        'fill color': np.where(s.selection, '#444444', '#FFFFFF'),
        'line color': np.where(variable_mask, '#FF0000', '#000000'),
        'line size': np.where(variable_mask, 1.25, 0.5)            
    })

    fig.circle(
        source=data_source,
        x='logfolds', y='pvalues',
        fill_color='fill color', line_color='line color', fill_alpha=0.5,
        size=8, line_width='line size'
    )
    for angle in [0, np.pi]:
        fig.ray(
            x=[-comparator.min_log_fold, +comparator.min_log_fold],
            y=[2, 2], angle=(np.pi/2 + angle), length=None,
            line_dash='dashed', line_width=1, line_color='#CC0000'
        )
        fig.ray(
            x=[0], y=[comparator.min_pvalue],
            angle=angle, length=None,
            line_dash='dashed', line_width=1, line_color='#CC0000'
        )
    bio.reset_output()
    bio.output_notebook()
    bpt.show(fig, **bokeh_args)