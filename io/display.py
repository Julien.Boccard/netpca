class NullMonitor:
    def state(s, text):
        pass

    def log(s, text):
        pass

class TerminalMonitor(NullMonitor):
    def state(s, text):
        pass

    def log(s, text):
        print(text)

class IPythonMonitor(NullMonitor):
    def __init__(s):
        import ipywidgets as ipw
        import IPython.display as ipd
        s.state_field = ipw.Text(layout=ipw.Layout(width='75%'))
        s.log_field = ipw.Textarea(layout=ipw.Layout(width='75%', height='24ex'))
        ipd.display(ipw.VBox([s.state_field, s.log_field]))

    def state(s, text):
        s.state_field.value = text

    def log(s, text):
        s.log_field.value = s.log_field.value + text + '\n'
