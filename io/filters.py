import re
import numpy as np

def never(_i, _value):
    return False, None

def as_is(_i, value):
    return True, value

def valid_float(_i, value):
    try:
        float_value = float(value)
        return True, float_value
    except ValueError:
        return False, np.nan

def start_at(index):
    def result(i, value):
        return i >= index, value
    return result

def valid_id(valid_regex='.*', exact=None, invalid_identifiers=None):
    regex = re.compile(valid_regex)
    def result(_i, value):
        if len(value) == 0:
            return False, value
        if invalid_identifiers is not None and value in invalid_identifiers:
            return False, value
        if regex.fullmatch(value) is None:
            return False, value
        if exact is not None and value != exact:
            return False, value
        return True, value
    return result

def map_values(value_dict, missing=None):
    def result(_i, o):
        if o not in value_dict:
            return False, missing
        else:
            return True, value_dict[o]
    return result